from tkinter import Text, Button, END, W, E, DISABLED, WORD
import msg as MSG


class Msg:
    def __init__(self, master):
        self.master = master
        master.title("clicka-msg")

        self.users_btn = {}
        self.users_dict = {}
        self.rows = 0
        self.entry = None
        self.sid = None
        self.user = None
        self.update()
        self.last_txt = ''
        self.txt_box = None
        self.txt_but = None
        self.users = None

    def set_sid(self, sid):
        self.sid = sid

    def set_users(self, users):
        if not self.users:
            self.users = users
        self.rows = 0
        for user in users:
            self.users_dict[user] = users[user]
            self.users_btn[user] = Button(
                self.master,
                height=1,
                text=users[user],
                command=lambda user=user: self.call(user))
            self.users_btn[user].grid(row=self.rows, column=0, sticky=W + E)
            self.rows += 1

    def show_msg(self, msgs, who):
        self.msg_rows = 0
        self.entry = Text(self.master, height=45, width=75, wrap=WORD)

        for msg in msgs:
            txt = None
            if msg[0]:
                txt = '{} :: {}\n>>> {}\n\n'.format('me', msg[1], msg[2])
            elif len(msg[2]) > 0:
                txt = '{} :: {}\n>>> {}\n\n'.format(who, msg[1], msg[2])
            else:
                txt = '[system] :: [wyświetlono]\n'

            self.entry.config(state="normal")
            self.entry.insert(END, txt)
            self.entry.config(state="disabled")
            self.entry.grid(row=0, column=1, rowspan=45, sticky=W + E)

        if not self.txt_box:
            self.txt_box = Text(self.master, height=20, width=75, wrap=WORD)
            self.txt_box.insert(END, self.last_txt)
            self.txt_box.grid(row=0, column=3, rowspan=20, sticky=W + E)
            self.txt_but = Button(
                self.master,
                height=1,
                text='Wyślij',
                command=lambda user=self.user: self.send(user))
            self.txt_but.grid(row=20, column=3, sticky=W + E)

    def call(self, user):
        if self.txt_box:
            self.last_text = self.txt_box.get("1.0", END)
        if self.user != user:
            for widget in self.master.winfo_children():
                widget.destroy()
            self.set_users(self.users)
            self.txt_box = None
            self.txt_but = None
            self.entry = None
        self.user = user
        self.show_msg(MSG.get(self.sid, user), self.users_dict[user])

    def send(self, user):
        txt = self.txt_box.get("1.0", END)
        MSG.send(self.sid, user, txt)
        self.txt_box.destroy()
        self.txt_but.destroy()
        self.txt_box = None
        self.txt_but = None
        self.last_txt = ''
        self.call(user)
        self.set_users(self.users)

    def update(self):
        if self.user:
            self.call(self.user)
        self.master.after(2000, self.update)
