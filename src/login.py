import requests


def get_session(login, passwd):
    cookies = {}
    data = {'login': login, 'passwd': passwd}
    uri = 'https://anx.nazwa.pl/xx.py?a=login_b'
    req = requests.post(uri, cookies=cookies, data=data)
    if len(req.history) == 0:
        return None
    sid = req.history[0].cookies['sid']
    return sid


def end_session(sid):
    cookies = {'sid': sid}
    data = {}
    uri = 'https://anx.nazwa.pl/xx.py?a=logout'
    requests.post(uri, cookies=cookies, data=data)
