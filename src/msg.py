import requests
import re


def send(sid, login, ctx):
    uri = 'https://anx.nazwa.pl/xx.py?a=mes_b&z={}'.format(login)
    cookies = {'sid': sid}
    data = {'content': ctx}
    req = requests.post(uri, cookies=cookies, data=data)
    return req


def raw_get(sid, login):
    uri = 'https://anx.nazwa.pl/xx.py?a=messages&z={}'.format(login)
    cookies = {'sid': sid}
    data = {}
    req = requests.get(uri, cookies=cookies, data=data)
    return req.content


def stop(idx, html, flen):
    if idx >= flen:
        return True

    seq = '<br></span><br>'

    cseq = ''
    for it in range(len(seq)):
        if idx + it >= flen:
            return True
        cseq = cseq + html[idx + it]

    if cseq == seq:
        return True

    return False


def get(sid, login):
    raw_html = raw_get(sid, login)
    html = str(raw_html.decode('utf-8'))
    idxs = [r.start() for r in re.finditer('<span style="color:', html)]
    msgs = []

    flen = len(html)

    for idx in idxs:
        me = None
        it = idx + 20
        if it < flen and html[it] == 'w':
            me = True
        elif it < flen and html[it] == 'l':
            me = False
        elif it < flen:
            me = False
            msgs.append((me, 'Wyświetlono', ''))
            break

        msg = ''

        start = False

        tt = ''
        nr_qtes = 0

        while it < flen and not stop(it, html, flen):
            if html[it] == '"':
                nr_qtes += 1
            elif nr_qtes == 2:
                tt = tt + html[it]
            elif html[it] == '>':
                start = True
            elif start:
                msg = msg + html[it]
            it += 1

        msgs.append((me, tt, msg))

    return msgs
