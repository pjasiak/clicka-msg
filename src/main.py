#!/usr/bin/python3

import gui
import json
import login
import os
import sys
import users
from tkinter import Tk


def main():
    config_path = os.path.join(os.getenv('HOME'), '.clicka.conf')

    try:
        with open(config_path, 'r') as f:
            data = json.load(f)
    except:
        print(
            'Sprawdź czy plik {} zawiera poprawną konfigurację'.format(
                config_path),
            file=sys.stderr)
        sys.exit(1)

    sid = login.get_session(data['login'], data['password'])
    if not sid:
        print(
            'Podano błędne dane logowania / albo serwer zdechł',
            file=sys.stderr)
        sys.exit(1)
    root = Tk()
    GUI = gui.Msg(root)
    GUI.set_sid(sid)
    GUI.set_users(users.get_users(sid))
    root.mainloop()
    login.end_session(sid)


if __name__ == '__main__':
    main()
