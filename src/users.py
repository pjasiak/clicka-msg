import requests
import re


def get_users_raw(sid):
    uri = 'https://anx.nazwa.pl/xx.cgi?a=friends'
    req = requests.post(uri, cookies={'sid': sid})
    return req.content


def get_users(sid):
    raw_html = get_users_raw(sid)
    html = str(raw_html.decode('utf-8'))
    idxs = [r.start() for r in re.finditer('_profile_', html)]
    users = {}

    flen = len(html)

    for idx in idxs:
        login = ''
        user = ''
        it = idx + 9
        while it < flen and html[it] != '&':
            login = login + html[it]
            it += 1
        it = it + 14
        while it < flen and html[it] != '<':
            user = user + html[it]
            it += 1
        users[login] = user

    return users
