INSTALL = install
LN = ln

SOURCES = $(CURDIR)/config.json \
		  $(CURDIR)/src/gui.py \
		  $(CURDIR)/src/login.py \
		  $(CURDIR)/src/main.py \
		  $(CURDIR)/src/msg.py \
		  $(CURDIR)/src/users.py \

.PHONY: all
all: install

.PHONY: install
install: $(SOURCES)
	$(INSTALL) -m 0644 $(CURDIR)/config.json $(HOME)/.clicka.conf
	$(LN) -s $(CURDIR)/src/main.py $(HOME)/bin/clicka-msg

.PHONY: uninstall
uninstall:
	@rm -f $(HOME)/.clicka.conf
	@rm -f $(HOME)/bin/clicka-msg
